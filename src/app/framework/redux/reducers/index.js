import { combineReducers } from "redux";

import AuthReducer from './auth.reducer';
import NavigatorReducer from './navigation.reducer';

export default combineReducers({
  AuthReducer,
  NavigatorReducer
});