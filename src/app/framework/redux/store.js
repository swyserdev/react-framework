import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import reducers from "./reducers";

export default createStore(
  reducers,
  {},
  applyMiddleware(logger(), thunk, promise())
);