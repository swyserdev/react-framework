import React from "react";
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router';

const styles = {
  links: {
    textDecoration: 'none'
  }
};

class AppDrawerComponent extends React.Component {
  constructor() {
    super();
  }

  toggleDrawer(e) {
    this.props.toggleDrawer();
  }

  render() {
    return (
      <Drawer
        open={this.props.drawerState}
        docked={false}
        onRequestChange={this.toggleDrawer.bind(this)}>
        <AppBar
          title="Menu"
          showMenuIconButton={false}
        />
        <Link to="/home" style={styles.links}><MenuItem onClick={this.toggleDrawer.bind(this)}>Home</MenuItem></Link>
      </Drawer>
    );
  }
}

export default AppDrawerComponent;