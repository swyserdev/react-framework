import React from "react";

import { AppBarComponent, AppDrawerComponent } from './';

class AppLayoutComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      drawerStatus: false
    };
  }

  toggleDrawer() {
    this.setState({
      drawerStatus: !this.state.drawerStatus
    });
  }

  render() {
    return (
      <div>
        <AppBarComponent toggleDrawer={this.toggleDrawer.bind(this)} />
        <AppDrawerComponent toggleDrawer={this.toggleDrawer.bind(this)} drawerState={this.state.drawerStatus} />
      </div>
    );
  }
}

export default AppLayoutComponent;