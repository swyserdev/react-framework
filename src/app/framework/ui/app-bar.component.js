import React from "react";

import AppBar from 'material-ui/AppBar';

class AppBarComponent extends React.Component {
  constructor() {
    super();
  }

  toggleDrawer(e) {
    this.props.toggleDrawer();
  }

  render() {
    return (
      <AppBar
        title="React Framework"
        iconClassNameRight="muidocs-icon-navigation-expand-more"
        onLeftIconButtonTouchTap={this.toggleDrawer.bind(this)}
      />
    );
  }
}

export default AppBarComponent;