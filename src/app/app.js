import React from 'react';
import ReactDOM from "react-dom";
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import Store from './framework/redux/store';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { HomeContainer, LoginContainer, AppContainer } from './containers';
import styles from './stylesheets/main.scss';
injectTapEventPlugin();

const AppMain = (
    <Provider store={Store}>
        <MuiThemeProvider> 
            <Router history={browserHistory}>
                <Route path={"/"} component={AppContainer}>
                    <IndexRoute component={LoginContainer} />
                    <Route path="home" component={HomeContainer}></Route>
                    <Route path={"login"} component={LoginContainer} />
                </Route>
            </Router>
        </MuiThemeProvider>
    </Provider>
);

ReactDOM.render(AppMain, document.getElementById('app'));