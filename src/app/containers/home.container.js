import React from 'react';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/FlatButton';
import { Link } from 'react-router';
import Box from 'react-layout-components';
import Paper from 'material-ui/Paper';

const styles = {
  cardImage: {
    width: 500
  },
  container: {
    marginTop: 50
  }
};

class HomeContainer extends React.Component {
  render() {
    return (
      <Box alignContent="center" justifyContent="center" flex="1 0 auto">
        <Paper zDepth={1} style={styles.container}>
          <Card>
            <CardMedia overlay={<CardTitle title="React Framework." subtitle="Hallo and welcome..." />}>
              <img style={styles.cardImage} src="images/welcome-min.jpg" />
            </CardMedia>
            <CardActions>
              <Box alignContent="center" justifyContent="center" flex="1 0 auto">
                <Link to="/home"><RaisedButton label="Home" primary={true} /></Link>
              </Box>
            </CardActions>
          </Card>
        </Paper>
      </Box>
    );
  }
}

export default HomeContainer;