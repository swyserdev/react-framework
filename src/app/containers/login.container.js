import React from 'react';
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import styles from '../stylesheets/containers/login.container.style.scss';

class LoginContainer extends React.Component {
    render() {
        return (
            <Card class="login-card">
                <CardMedia class="login-media">
                    <img src="images/login-header.png" />
                </CardMedia>
                <CardText>
                    <div>
                        <TextField
                            class="login-textbox"
                            hintText="Email"
                            floatingLabelText="Email"
                            fullWidth={true} />
                    </div>
                    <div>
                        <TextField
                            class="login-textbox"
                            hintText="Password"
                            floatingLabelText="Password"
                            type="password"
                            fullWidth={true} />
                    </div>
                </CardText>
                <CardActions class="login-actions">
                    <RaisedButton primary={true} label="Login" />
                    <RaisedButton secondary={true} label="Clear" class="login-button-clear" />
                </CardActions>
            </Card>
        );
    }
}

export default LoginContainer;