import React from 'react';
import { AppLayoutComponent } from '../framework/ui';
import styles from '../stylesheets/containers/app.container.style.scss';

const style = {
  container: {
    margin: 20
  }
};

class AppContainer extends React.Component {
  render() {
    return (
      <div class="make-tall">
        <AppLayoutComponent />
        {this.props.children}
      </div>
    );
  }
}

export default AppContainer;