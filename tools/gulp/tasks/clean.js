const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const clean = require('gulp-clean');

gulp.task('clean', () => {
  return gulp.src('dist/')
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(clean());
});