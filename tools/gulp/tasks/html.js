const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const htmlmin = require('gulp-htmlmin');

gulp.task('minify-html', () => {
  return gulp.src('src/index.html')
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist/'));
});