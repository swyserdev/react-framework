const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const webpack = require("webpack-stream");

gulp.task('webpack', () => {
  return gulp.src(['src/app/app.js'])
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(webpack(require('../../webpack/webpack.config.js').client))
    .pipe(gulp.dest('dist'));
});