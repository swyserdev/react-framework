const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const cssmin = require('gulp-cssmin');

gulp.task('minify-css', () => {
  return gulp.src('src/css/*.css')
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(cssmin())
    .pipe(gulp.dest('dist/css/'));
});