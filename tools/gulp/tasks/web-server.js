const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const webserver = require('gulp-webserver');
const settings = require('../config/settings.js');

gulp.task('serve', () => {
  return gulp.src('dist/')
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(webserver({
      host: settings().server.host,
      port: settings().server.port,
      livereload: true,
      directoryListing: false
    }));
});