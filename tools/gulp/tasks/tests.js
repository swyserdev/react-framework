var gulp = require('gulp');
var jest = require('jest-cli');

gulp.task('unit-tests', (done) => {
  return jest.runCLI({
    config: {
      rootDir: './src/app/tests',
      moduleNameMapper: {
        '^.+\\.(css|scss)$': './tools/SassStub.js'
      },
    }
  }, './', () => {
    done();
  });
});