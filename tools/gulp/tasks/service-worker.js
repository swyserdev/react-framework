const gulp = require('gulp');
const plumber = require('gulp-plumber');
const swPrecache = require('sw-precache');

gulp.task('gsw', (callback) => {
  return swPrecache.write('dist/service-worker.js', {
    cacheId: 'ConferenceIn.',
    staticFileGlobs: [
      'dist/**/*.{js,html,css,png,svg,jpg,gif,json}'
    ],
    stripPrefix: 'dist'
  }, callback);
});