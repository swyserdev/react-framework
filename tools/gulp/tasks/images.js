const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');

gulp.task('images', () => {
  return gulp.src('src/assets/images/**/*.*')
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(gulp.dest('dist/images/'));
});