const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');

gulp.task('favicon', () => {
  return gulp.src('src/favicon.ico')
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(gulp.dest('dist/'));
});