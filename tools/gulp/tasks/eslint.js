const gulp = require('gulp');
const plumber = require('gulp-plumber');
const eslint = require('gulp-eslint');

gulp.task('lint', () => {
  return gulp.src('src/**/*.js')
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format());
});