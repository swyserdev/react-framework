const gulp = require('gulp');
const open = require('open');
const settings = require('../config/settings.js')

gulp.task('openbrowser', () => {
  return open('http://' + settings().server.host + ':' + settings().server.port)
});