const gulp = require('gulp');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');

gulp.task('mainfest', () => {
  return gulp.src('src/manifest.json')
    .pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
    .pipe(gulp.dest('dist/'));
});