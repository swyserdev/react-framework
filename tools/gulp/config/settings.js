const settings = {
  server: {
    host: 'localhost',
    port: '8080'
  }
};

module.exports = () => {
  return settings;
};