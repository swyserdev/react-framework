var ExtractTextPlugin = require('extract-text-webpack-plugin');

var config = {
  client: {
    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader'
        },
        {
          test: /\.scss?$/,
          loader: 'style!css!sass'
        }
      ]
    },
    output: {
      filename: 'app.js'
    }
  },
  server: {}
};

module.exports = config;