# React Framework

React framework to easily get started with a project.

## Getting Started

To get started with this project follow these steps:
1. Clone the repo to a folder using: git clone https://github.com/SwyserDev/react-framework.git
2. Navigate to folder containing cloned repo.
3. Make sure that you have yarn installed globally then run the command: **yarn install**
4. Run the following gulp commands in the exact same order:
* gulp
5. Sit back and enjoy the framework, or begin coding like a madman!

### Prerequisites

* Node globally installed is required. [Node](https://nodejs.org/en/download/package-manager/)
* Yarn package manager need to be installed globally. [Yarn](https://yarnpkg.com/lang/en/docs/install/)

## Notes about build

Live reload and watch has been added to project so no need to rerun gulp when files inside src folder has been changed.

## Technologies set

1. React-redux
2. Material-UI
3. WebPack
* Babel
* Sass
4. Gulp

## Deployment

The Dockerfile that is included needs to be changed, please do not use.

## Versioning

Who knows...

## Authors

Francois van der Merwe (swyser.co.za)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

The solo adventures of a midnight code scrolling geek wizard...