const gulp = require('gulp');
const requireDir = require('require-dir');
const FwdRef = require('undertaker-forward-reference');
const watch = require('gulp-watch');
const notify = require("gulp-notify");

gulp.registry(FwdRef());

requireDir('./tools/gulp/tasks', { recurse: true });

gulp.task('default',
  gulp.series(
    'clean',
    gulp.parallel('favicon', 'minify-html', 'images', 'webpack', 'mainfest'),
    'gsw',
    'serve',
    'unit-tests',
    'openbrowser',
    'lint',
    'watch'
  )
);

gulp.task('watch', () => {
  return gulp.watch('./src/**/*.*', 
    gulp.series(
      gulp.parallel('minify-html', 'images', 'webpack', 'lint', 'unit-tests')
    )
  );
});